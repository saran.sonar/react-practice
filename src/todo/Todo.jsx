import { useFormik } from 'formik';
import * as yup from 'yup';
import { useState } from "react";

function Todo(){
    const [todoForm,setTodoForm]=useState({
        id:'',
        task:'',
        description:''
    })
    
    const [tableData,setTableData]=useState([])
    
    
    const onSubmit = (values)=>{
        if(values['id']){
            setTableData([...tableData.filter(el=>el['id']!==values['id']),values])
        }else{
            setTableData([...tableData,values])
        }
        formikForm.resetForm();
    }
    
    const onValidation = yup.object().shape({
        id: yup.number().required('Id is Required!'),
        task: yup.string().required('Title is Required!'),
        description: yup.string().required('Description is Required!')
    })
    
    const formikForm= useFormik({
        initialValues:todoForm,
        onSubmit,
        validationSchema:onValidation
    })

    const editData=(el)=>{
        formikForm.setFieldValue('id', el.id).then(res => {})
        formikForm.setFieldValue('task', el.task).then(res => {})
        formikForm.setFieldValue('description', el.description).then(res => {})
    }

    const deleteData=(el)=>{
        setTableData(tableData.filter(data=>data['id']!==el['id']))
    }

    return(<>
    <form onSubmit={formikForm.handleSubmit}>
        <label htmlFor="id">ID :</label>
        <input type="number" name="id" id="id" value={formikForm?.values.id} onBlur={formikForm.handleChange} onChange={formikForm.handleChange} />
        {formikForm?.touched?.id && formikForm?.errors?.id} <br/>
        <label htmlFor="task">Task :</label>
        <input type="text" name="task" id="task" value={formikForm?.values.task}  onBlur={formikForm.handleChange} onChange={formikForm.handleChange} />
        {formikForm?.touched?.task && formikForm?.errors?.task}<br/>
        <label htmlFor="description">Description :</label>
        <input type="text" name="description" id="description"  value={formikForm?.values.description}  onBlur={formikForm.handleChange} onChange={formikForm.handleChange}/>
        {formikForm?.touched?.description && formikForm?.errors?.description}<br/>
        <button type="submit" >Save</button>
    </form>

    <br /><br /><br />
    <div><u><h3>Todo Table</h3></u></div>
    <table>
        <thead>
            <tr >
                <th style={{textAlign:'center'}}>Task</th>
                <th style={{textAlign:'center'}}>Description</th>
                <th style={{textAlign:'center'}}>Action</th>
            </tr>
        </thead>
        <tbody>
                { 
                tableData.map((el,i) => {
                  return( 
                    <tr key={i}>

                    <td >{el.task}</td>
                    <td >{el.description}</td>
                    <td>
                        <button onClick={()=>editData(el)}>edit</button>
                        <button onClick={()=>deleteData(el)}>delete</button>
                    </td>
                    </tr>
                    )
                })
                }
        </tbody>
    </table>
    </>)
}

export default Todo;